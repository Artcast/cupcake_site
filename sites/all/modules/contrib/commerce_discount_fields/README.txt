
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage


INTRODUCTION
------------

  Current Maintainer: Alexandru Mreana <mreanaa@gmail.com>

  Allows the creation of commerce product discounts based on a custom field.


INSTALLATION
------------

  1. Extract the 'commerce_discount_fields' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).
  2. Enable commerce_discount_fields from admin/modules 
  ("Commerce (contrib)" section).
  3. Proceed to admin/commerce/store/discounts/fields where you can select
  the field you wish to use as an option for commerce discounts.

USAGE
----

  To use simply go to create the desired discount using the custom field you've
  selected in the admin area.
